//Comentario de una sola linea 

/*
Comentario multilinea
*/

//Programacion orientada a objetos
//Actividad 02:Tipos de datos
//By: Oliver

public class Actividad02 {

	public static void main (String[] args) {
	
		//Variables *****
		byte edad = 12;
		System.out.println(edad);
		
		//Constante *****
		
		final float PI = 3.14f;
		System.out.println("Yo soy PI: " + PI);


		//Flotante
		float peso = 75.5f; 
		double estatura = 1.74d;
		System.out.println(peso);
		System.out.println(estatura);

		// Booleano
		boolean casado = true;
		System.out.println(casado);

		// Caracteres
		char primerLetraNombre = 'F';
		System.out.println(primerLetraNombre);

		String nombre = "Oliver Alexander";
		System.out.println(nombre);

		Integer var = null;
		System.out.println(var);
		

	}

}